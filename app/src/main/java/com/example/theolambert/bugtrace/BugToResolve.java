package com.example.theolambert.bugtrace;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class BugToResolve extends AppCompatActivity implements AsyncResponse, AdapterView.OnItemSelectedListener {
    private String id_ = ""; //ID DU USER
    private String Admin_ = ""; //IF IS ADMIN
    private String method = "BugPost"; //METHODE POUR L'ASYNCTASK
    private String[] idBug = null; //ID DU BUG
    private String[] nameBug = null; //NAME DU BUG
    private String[] Statement = null; // STATEMENT DU BUG
    private ArrayList<String> items = null; //ARRAY LIST DE LA FUTUR LISTVIEW
    private String Current_id = null; //ID DU BUG A RENVOYE
    private Spinner spinner; //SPINNER DU STATEMENT
    private static final String[]paths = {"Select","New", "Accept", "Resolved", "Closed"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BackgroundTasks asyncTask = new BackgroundTasks(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bugtoresolve);
        Intent myIntent = getIntent();
        id_ = myIntent.getStringExtra("id");
        Admin_ = myIntent.getStringExtra("Admin");
        asyncTask.delegate = this;
        asyncTask.execute(method, id_);

        //SET DU SPINNER POUR LE STATEMENT
        spinner = (Spinner)findViewById(R.id.spinnerbugReport);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BugToResolve.this,
                android.R.layout.simple_spinner_item, paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(BugToResolve.this);
    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg) {
        //ON PARSE LE JSON EN OBJET 'BUGObj' POUR POUVOIR ACCEDER AUX KEY/VALUES DES BUGS
        if(FLAG == 15){
            final ListView lv = (ListView) findViewById(R.id.ListBug);
            if (code.equals("0")) {
                Log.d("TAG JSON ----->", " " + result);
                try {
                    JSONArray BUG = new JSONArray(result);
                    JSONObject BUGObj;
                    idBug = new String[BUG.length()];
                    nameBug = new String[BUG.length()];
                    Statement = new String[BUG.length()];
                    items = new ArrayList<>();
                    for (int i = 0; i < BUG.length(); i++) {
                        BUGObj = BUG.getJSONObject(i);
                        nameBug[i] = BUGObj.get("libelle_inc").toString();
                        idBug[i] = BUGObj.get("id_inc").toString();
                        Statement[i] = BUGObj.get("statement_inc").toString();
                        String Priority = BUGObj.get("priority_inc").toString();
                        items.add(idBug[i] + "." + nameBug[i]+" - "+Statement[i]+" - "+Priority);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, items);
                assert lv != null;
                lv.setAdapter(mArrayAdapter);
                lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    TextView txtBUG = (TextView) findViewById(R.id.txtbug);
                    TextView txtSWITCH = (TextView) findViewById(R.id.txtbugSwitch);

                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        //SI L'USER EST LAMBDA ON AFFICHE LE BOUTON ET LA METHODE PAR DEFAULT POUR POUVOIR GERER L'ETAT
                        Current_id = idBug[position];
                        txtSWITCH.setText(nameBug[position] + " ?");
                        txtSWITCH.setVisibility(View.VISIBLE);

                        findViewById(R.id.spinnerbugReport).setVisibility(View.VISIBLE);
                        if(Admin_.equals("1")){
                            //SI L'USER EST ADMIN ON AFFICHE LE BOUTON ET LA METHODE PAR DEFAULT POUR POUVOIR SUPPRIMER UN BUG
                            findViewById(R.id.delete).setVisibility(View.VISIBLE);
                            txtBUG.setText(nameBug[position] + " ?");
                            txtBUG.setVisibility(View.VISIBLE);
                            findViewById(R.id.deletebug).setVisibility(View.VISIBLE);
                            findViewById(R.id.deletebugtr).setVisibility(View.INVISIBLE);
                        }
                    }
                });
            } else if (code.equals("2")) {
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        finish();
                    }
                }, 2000);
            }
        }
        if(FLAG == 16 || FLAG == 17){
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
    }



    @Override
    //METHODE ONCLICK QUI VA DEFENIR L'ETAT DU BUG DANS LE SPINNER
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String resEtat = parent.getItemAtPosition(position).toString();
        if(resEtat.equals("Select")){
            //
        }
        if (!resEtat.equals("Select") && Current_id != null)  {
            BackGroundMethodChangeState(resEtat, Current_id);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }

    //ON GENERERE UNE BOIE DE DIALOGUE AFIN DE SAVOIR SI L'ADMIN VEUT VRAIMENT SUPPRIMER LE BUG
    public void onClickDisableBug(View view){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        BackGroundMethodDisableBug(Current_id);
                        //ON RECUPERE LE CURRENT ID DU BUG, CELUI SUR LEQUEL ON A 'ITEM'CLICK'...
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };
        //L'ADMIN CHOISI OUI/NON POUR CONFIRMER LA SUPRESSION
        AlertDialog.Builder builder = new AlertDialog.Builder(BugToResolve.this);
        builder.setTitle("Supress This Bug").setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

    }
    //METHODE POUR SUPPRIMER LE BUG EN COURS
    public void BackGroundMethodDisableBug(String Bugid){
        String method = "DeleteBug";
        BackgroundTasks asyncTask =new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method,Bugid);
    }

    //METHODE POUR CHANGER L'ETAT D'UN BEGA
    public void BackGroundMethodChangeState(String State, String id){
       String method = "StateBug";
        BackgroundTasks asyncTask =new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method,State, id);
    }

    //METHODE POUR REFRESH LA LISTVIEW
    public void RefreshBugs(View view){
        finish();
        startActivity(getIntent());
    }
}