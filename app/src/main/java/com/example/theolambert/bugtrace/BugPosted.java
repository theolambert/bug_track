package com.example.theolambert.bugtrace;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BugPosted extends AppCompatActivity implements AsyncResponse {
    private String id_ = "";// ID OF THE ITENT
    private String Admin_ = ""; //ADMIN RESULT OF THE INTENT
    private String[] idBug = null; //TAB CONTAIN ID BUGS
    private String[] nameBug = null; //TAB CONTAIN NAME BUG
    private String[] Statement = null; // TAB CONTAIN SATEMENT BUG
    private ArrayList<String> items = null; //FUTUR LISTVIEW
    public String method = "MyBugs"; //METHOD SENT TO THE ASYNCTASK

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        BackgroundTasks asyncTask = new BackgroundTasks(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bugposted);
        Intent myIntent = getIntent();
        id_ = myIntent.getStringExtra("id"); //ID DU USER
        Admin_ = myIntent.getStringExtra("Admin"); //SAVOIR SI IL EST ADMIN
        asyncTask.delegate = this;
        asyncTask.execute(method, id_); //LANCEMEMENT DE L'ASYNCTASK
    }

    @Override
    //ON RESULT DES ASYNCTASK
    public void processFinish(String code, String result, int FLAG, String msg) {
        final ListView lv = (ListView) findViewById(R.id.ListMyBug);
        if (code.equals("0")) {
            try {
                //JSON PARSER ANS UNE BOUCLE POUR RECUPERER TOUTES LES CLE/VALEUR DE CHAQUE OBJET CONTENU DANS LE JSON
                JSONArray BUG = new JSONArray(result);
                JSONObject BUGObj;
                idBug = new String[BUG.length()];
                nameBug = new String[BUG.length()];
                Statement = new String[BUG.length()];
                items = new ArrayList<>();
                for (int i = 0; i < BUG.length(); i++) {
                    BUGObj = BUG.getJSONObject(i);
                    nameBug[i] = BUGObj.get("libelle_inc").toString(); //STOCKAGE DU NOM DU BUG
                    idBug[i] = BUGObj.get("id_inc").toString(); //STOCKAGE DE L'ID DU BUG
                    Statement[i] = BUGObj.get("statement_inc").toString(); //STOCKAGE DE L'ETAT DU BUG
                    items.add(idBug[i] + "." + nameBug[i] + " - Statement : " + Statement[i]); //ON AJOUTE DANS L'ADAPTER QUI SERA LA LISTVIEW
                }
            } catch (JSONException e){e.printStackTrace();}
            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, items);
            assert lv != null;
            lv.setAdapter(mArrayAdapter); //ON SET L'ADAPTER DANS LA LISTVIEW
        }
        else if (code.equals("2")) {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                }
            }, 2000);
            //SI LE JSON RENVOYE EST VIDE, ON QUITTE L'ACTIVITY
        }
    }
}