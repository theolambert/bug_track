package com.example.theolambert.bugtrace;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProjectModule extends AppCompatActivity implements AsyncResponse {
    private String id_module = "", name_module= "", progress_module = "", description_module = "", idProj = "", ProjectnameForModule = "";
    private int count = 0;
    String temp = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_module);
        Intent myIntent = getIntent();
        idProj = myIntent.getStringExtra("idProj");
            ProjectnameForModule = myIntent.getStringExtra("projectname");
            String method = "ModuleProject";
            BackgroundTasks asyncTask = new BackgroundTasks(this);
            asyncTask.delegate = this;
            asyncTask.execute(method, idProj);
    }

    @Override
    public void processFinish(String code, final String result, int FLAG, String msg) {
        if (code.equals("0")) {
            JSONArray jsonArray = null;
            try {
                jsonArray = new JSONArray(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            try {
                JSONArray users = new JSONArray(result);
                JSONObject user;
                for (int i = 0; i < users.length(); i++) {
                    user = users.getJSONObject(i);
                    id_module = user.get("id").toString();
                    name_module += (i+1) +" for "+user.get("name").toString()+" / ";
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            count = jsonArray.length();
            final EditText Res = new EditText(this);
            new AlertDialog.Builder(this)
                    .setTitle("Module")
                    .setMessage("Select the module :  "+name_module)
                    .setView(Res)
                    .setPositiveButton("View My Module", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            String responseModule = Res.getText().toString();
                            try {
                                JSONArray users = new JSONArray(result);
                                JSONObject user;
                                int res = Integer.parseInt(responseModule);
                                user = users.getJSONObject(res-1);
                                id_module = user.get("id").toString();
                                name_module = user.get("name").toString();
                                progress_module = user.get("progress").toString();
                                description_module = user.get("description").toString();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            TextView idProj_ = (TextView)findViewById(R.id.idProjModule);
                            TextView nameModule_ = (TextView)findViewById(R.id.nameModule);
                            TextView descModule_ = (TextView)findViewById(R.id.DescriptionModule);
                            TextView avancementModule_ = (TextView)findViewById(R.id.Progressmodule);
                            TextView idModule_ = (TextView)findViewById(R.id.idModule);
                            TextView ProjectTitleForModule_ = (TextView)findViewById(R.id.TitleProjectModule);
                            idProj_.setText(idProj);
                            nameModule_.setText(id_module);
                            descModule_.setText(name_module);
                            avancementModule_.setText(progress_module);
                            idModule_.setText(description_module);
                            ProjectTitleForModule_.setText(ProjectnameForModule);
                            ProgressBar Prog = (ProgressBar) findViewById(R.id.progressBar1);
                            Prog.setVisibility(View.VISIBLE);
                            Prog.setProgress((int)Double.parseDouble(progress_module));

                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            Toast.makeText(ProjectModule.this, "Nothing to show..!", Toast.LENGTH_LONG).show();
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                public void run() {
                                    finish();
                                }
                            }, 1500);
                        }
                    })
                    .show();
        }
        else if (code.equals("2")) {
            Toast.makeText(ProjectModule.this, "Nothing to show here..!", Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                }
            }, 1500);
        }
    }

    public void OnClickChangeModule(View view){
        finish();
        startActivity(getIntent());
    }
}
