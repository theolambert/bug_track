package com.example.theolambert.bugtrace;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by theolambert on 03/07/16.
 */

//DEFINITON DE LA CLASSE
public class BackgroundTasks extends AsyncTask<String, Void, String> {
    public AsyncResponse delegate;
    Context ctx;
    BackgroundTasks(Context ctx){
        this.ctx = ctx;
    }
    private int FLAG = 0;

    @Override
    protected void onPreExecute() {

    }

    @Override
    //DEFINITON DES URL QUI VONT SERVIR A SE CONNECTER AUX WEBSERVICES
    protected String doInBackground(String... params) {
        String register_url = "http://vps295091.ovh.net/php/register.php";
        String login_url = "http://vps295091.ovh.net/php/login.php";
        String team_url = "http://vps295091.ovh.net/php/myteam.php";
        String project_url = "http://vps295091.ovh.net/php/myprojects.php";
        String update_url = "http://vps295091.ovh.net/php/myupdate.php";
        String password_url = "http://vps295091.ovh.net/php/mypassword.php";
        String bug_report_url = "http://vps295091.ovh.net/php/reportmybug.php";
        String delete_url = "http://vps295091.ovh.net/php/deleteUser.php";
        String allteam_url = "http://vps295091.ovh.net/php/allTeam.php";
        String detailsproject_url = "http://vps295091.ovh.net/php/projectDetails.php";
        String module_url = "http://vps295091.ovh.net/php/modulesByProject.php";
        String deleteproj_url = "http://vps295091.ovh.net/php/DeleteProject.php";
        String deleteuser_url = "http://vps295091.ovh.net/php/deleteUser.php";
        String allInfoUser_url = "http://vps295091.ovh.net/php/allInfoUser.php";
        String bugposted_url = "http://vps295091.ovh.net/php/BugToResolve.php";
        String deleteincident_url = "http://vps295091.ovh.net/php/DeleteIncident.php";
        String updateincident_url = "http://vps295091.ovh.net/php/updatebug.php";
        String mybugs_url = "http://vps295091.ovh.net/php/BugPosted.php";
        String yourmodule_url = "http://vps295091.ovh.net/php/YourModule.php";
        //DEFINITON DE TOUTE LES METHODES APPELER PAR LES ACTIVITY

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        String method = params[0];
        if (method.equals("register")){
            FLAG = 1;
            String nom = params[1];
            String prenom = params[2];
            String mdp = params[3];
            String mail = params[4];
            String team = params[5];
            String admin = params[6];
            try {
                URL url = new URL(register_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("nom", "UTF-8") + "=" +URLEncoder.encode(nom,"UTF-8")+"&" +
                        URLEncoder.encode("prenom", "UTF-8") + "=" +URLEncoder.encode(prenom,"UTF-8")+"&" +
                        URLEncoder.encode("mdp", "UTF-8") + "=" +URLEncoder.encode(mdp,"UTF-8")+"&" +
                        URLEncoder.encode("mail", "UTF-8") + "=" +URLEncoder.encode(mail,"UTF-8")+"&" +
                        URLEncoder.encode("team", "UTF-8") + "=" +URLEncoder.encode(team,"UTF-8")+"&" +
                        URLEncoder.encode("admin", "UTF-8") + "=" +URLEncoder.encode(admin,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("login")){
            FLAG = 2;
            String mail = params[1];
            String mdp = params[2];
            try {
                URL url = new URL(login_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("mail", "UTF-8") + "=" +URLEncoder.encode(mail,"UTF-8")+"&" +
                        URLEncoder.encode("mdp", "UTF-8") + "=" +URLEncoder.encode(mdp,"UTF-8")+"&";
                return URL(data, bufferwriter, httpUrlConnection, OS);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("team")){
            try {
                String id_equipe = params[1];
                FLAG = 3;
                URL url = new URL(team_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_equipe", "UTF-8") + "=" +URLEncoder.encode(id_equipe,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("project")){
            try {
                String id_user = params[1];
                FLAG = 4;
                URL url = new URL(project_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("update")){
            try {
                String id_user = params[1];
                String nom_user = params[2];
                String prenom_user = params[3];
                String team_user = params[4];

                FLAG = 5;
                Log.d("update", "USER ID : "+id_user);
                URL url = new URL(update_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8")+"&" +
                        URLEncoder.encode("nom_user", "UTF-8") + "=" +URLEncoder.encode(nom_user,"UTF-8")+"&" +
                        URLEncoder.encode("prenom_user", "UTF-8") + "=" +URLEncoder.encode(prenom_user,"UTF-8")+"&" +
                        URLEncoder.encode("team_user", "UTF-8") + "=" +URLEncoder.encode(team_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("pass")){
            try {
                String id_user = params[1];
                String mdp_user = params[2];
                FLAG = 6;
                URL url = new URL(password_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8")+"&" +
                        URLEncoder.encode("mdp_user", "UTF-8") + "=" +URLEncoder.encode(mdp_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("Bug_report")){
            try {
                String title = params[1];
                String desc = params[2];
                String state = params[3];
                String id_user = params[4];
                String priority = params[5];
                String avancement = params[6];
                String userReport = params[7];
                String resModule = params[8];
                FLAG = 7;
                URL url = new URL(bug_report_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("libbele_bug", "UTF-8") + "=" +URLEncoder.encode(title,"UTF-8")+"&" +
                        URLEncoder.encode("desc_bug", "UTF-8") + "=" +URLEncoder.encode(desc,"UTF-8")+"&" +
                        URLEncoder.encode("state_bug", "UTF-8") + "=" +URLEncoder.encode(state,"UTF-8")+"&" +
                        URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8")+"&" +
                        URLEncoder.encode("priority_bug", "UTF-8") + "=" +URLEncoder.encode(priority,"UTF-8")+"&" +
                        URLEncoder.encode("avancement_bug", "UTF-8") + "=" +URLEncoder.encode(avancement,"UTF-8")+"&" +
                        URLEncoder.encode("user_assign_bug", "UTF-8") + "=" +URLEncoder.encode(userReport,"UTF-8")+"&" +
                        URLEncoder.encode("res_module_bug", "UTF-8") + "=" +URLEncoder.encode(resModule,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("Delete")){
            try {
                String id_user = params[1];
                FLAG = 8;
                URL url = new URL(delete_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("allTeam")){
            try {
                FLAG = 9;
                URL url = new URL(allteam_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                InputStream IN = httpUrlConnection.getInputStream();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
                String res = "";
                String line = "";
                while((line = bufferedReader.readLine()) != null){
                    res+=line;
                }
                bufferedReader.close();
                IN.close();
                httpUrlConnection.disconnect();
                return res;

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("alldetailsProject")){
            try {
                String id_proj = params[1];
                FLAG = 10;
                URL url = new URL(detailsproject_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_proj", "UTF-8") + "=" +URLEncoder.encode(id_proj,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("ModuleProject")){
            try {
                String id_proj = params[1];
                FLAG = 11;
                URL url = new URL(module_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_proj", "UTF-8") + "=" +URLEncoder.encode(id_proj,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("DeleteProj")){
            try {
                String id_pro = params[1];
                FLAG = 12;
                URL url = new URL(deleteproj_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_pro", "UTF-8") + "=" +URLEncoder.encode(id_pro,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("DeleteUser")){
            try {
                String id_user = params[1];
                FLAG = 13;
                URL url = new URL(deleteuser_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("CurrentInfo")){
            try {
                String id_user = params[1];
                FLAG = 14;
                URL url = new URL(allInfoUser_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("BugPost")){
            try {
                String id_user = params[1];
                FLAG = 15;
                URL url = new URL(bugposted_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("DeleteBug")){
            try {
                String id_bug = params[1];
                FLAG = 16;
                URL url = new URL(deleteincident_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_inc", "UTF-8") + "=" +URLEncoder.encode(id_bug,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("StateBug")){
            try {
                String statement_inc = params[1];
                String id_inc = params[2];
                FLAG = 17;
                URL url = new URL(updateincident_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_inc", "UTF-8") + "=" +URLEncoder.encode(id_inc,"UTF-8")+"&" +
                        URLEncoder.encode("statement_inc", "UTF-8") + "=" +URLEncoder.encode(statement_inc,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ///////////////////////////////////
        if (method.equals("MyBugs")){
            try {
                String id_user = params[1];
                FLAG = 18;
                URL url = new URL(mybugs_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_user", "UTF-8") + "=" +URLEncoder.encode(id_user,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (method.equals("YourModule")){
            try {
                String id_proj = params[1];
                FLAG = 19;
                URL url = new URL(yourmodule_url);
                HttpURLConnection httpUrlConnection = (HttpURLConnection)url.openConnection();
                httpUrlConnection.setRequestMethod("POST");
                httpUrlConnection.setDoOutput(true);
                httpUrlConnection.setDoInput(true);
                OutputStream OS = httpUrlConnection.getOutputStream();
                BufferedWriter bufferwriter = new BufferedWriter(new OutputStreamWriter(OS,"UTF-8"));
                String data = URLEncoder.encode("id_pro", "UTF-8") + "=" +URLEncoder.encode(id_proj,"UTF-8");
                return URL(data, bufferwriter, httpUrlConnection, OS);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    public void onPostExecute(String result){
        Log.d("OPE", "onPostExecute: "+result);
            String code = "1";
            String msg = null;
            try {
                JSONArray users = new JSONArray(result);
                JSONObject user;
                user = users.getJSONObject(0);
                code = user.get("code").toString();
                msg = user.get("msg").toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (code.equals("0"))
            {
                Log.d("ASSYNC TASK SUCCESS -> ", msg);
                delegate.processFinish(code, result, FLAG, msg);
            }
            if (code.equals("2"))
            {
            Log.d("ASSYNC TASK WARNING -> ", msg);
            delegate.processFinish(code, result, FLAG, msg);
            }
            else if (code.equals("1"))
            {
                Toast.makeText(ctx,"msg = "+msg, Toast.LENGTH_LONG).show();
            }
    }

    public String URL(String data, BufferedWriter bufferwriter, HttpURLConnection httpUrlConnection, OutputStream OS) throws IOException {
        bufferwriter.write(data);
        bufferwriter.flush();
        bufferwriter.close();
        OS.close();
        InputStream IN = httpUrlConnection.getInputStream();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(IN, "iso-8859-1"));
        String res = "";
        String line = "";
        while((line = bufferedReader.readLine()) != null){
            res+=line;
        }
        bufferedReader.close();
        IN.close();
        httpUrlConnection.disconnect();
        return res;
    }
}
