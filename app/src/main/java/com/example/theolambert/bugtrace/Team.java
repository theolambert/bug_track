package com.example.theolambert.bugtrace;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Team extends AppCompatActivity implements AsyncResponse {
    private String method = "team";
    private String idUserToDelete = null, team = null, mail = null;;
    private ArrayList<String> items = null;
    private String team_info = null;
    private String [] id_ = null;
    public String [] name_ = null;
    private String Admin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Intent myIntent = getIntent();
        if(myIntent != null) {
            team_info = myIntent.getStringExtra("team");
            Admin = myIntent.getStringExtra("Admin");
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_team);
            BackgroundTasks asyncTask =new BackgroundTasks(this);
            asyncTask.delegate = this;
            asyncTask.execute(method, team_info);
        }
    }

    @Override
    public void processFinish(String code, String result, int FLAG, final String msg){
        if(FLAG == 3){
            final ListView lv = (ListView)findViewById(R.id.ListViewTest);
            TextView title = (TextView)findViewById(R.id.myTitle);
            if (code.equals("0")){
                try {
                    JSONArray users = new JSONArray(result);
                    JSONObject user;
                    id_ = new String[users.length()];
                    name_ = new String[users.length()];
                    items = new ArrayList<>();
                    for (int i = 0; i < users.length(); i++) {
                        user = users.getJSONObject(i);
                        id_[i] = user.get("id").toString();
                        team = user.get("team").toString();
                        name_[i] = user.get("name").toString();
                        mail = user.get("mail").toString();
                        items.add(mail);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, items);
                assert lv != null;
                lv.setAdapter(mArrayAdapter);
                assert title != null;
                title.setText(team);
                if(Admin.equals("1")){
                    ImageView img = (ImageView)findViewById(R.id.crossDeleteUser);
                    TextView info = (TextView)findViewById(R.id.infoDel);
                    img.setVisibility(View.VISIBLE);
                    info.setVisibility(View.VISIBLE);
                    lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            idUserToDelete = id_[position];
                            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    switch (which){
                                        case DialogInterface.BUTTON_POSITIVE:
                                            BackGroundMethodDisableUser(idUserToDelete);
                                            break;

                                        case DialogInterface.BUTTON_NEGATIVE:
                                            //No button clicked
                                            break;
                                    }
                                }
                            };

                            AlertDialog.Builder builder = new AlertDialog.Builder(Team.this);
                            builder.setTitle("Supress This User").setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                                    .setNegativeButton("No", dialogClickListener).show();
                        }
                    });
                }
            }
            else if (code.equals("2")){
                title.setText("0 result found !");
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        finish();
                    }
                }, 2000);
            }

        }
        if(FLAG == 13) {
            Toast.makeText(Team.this, msg, Toast.LENGTH_LONG).show();
        }

    }

    public void BackGroundMethodDisableUser(String id){
        String id_user = id;
        String method = "DeleteUser";
        BackgroundTasks asyncTask =new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method,id_user);
    }

    public void RefreshTeam(View view){
        finish();
        startActivity(getIntent());
    }
}