package com.example.theolambert.bugtrace;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ProjectInfo extends AppCompatActivity implements AsyncResponse {
    private String avancementProj = "", descrProj = "", idProj = "", projectname = "", ChefProj = "", Admin = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_info);
        Intent myIntent = getIntent();
        idProj = myIntent.getStringExtra("idProj");
        Admin = myIntent.getStringExtra("Admin");
        String method = "alldetailsProject";
        BackgroundTasks asyncTask = new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method, idProj);
    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg) {
        if(FLAG == 10){
                try {
                    JSONArray users = new JSONArray(result);
                    JSONObject user;
                    user = users.getJSONObject(0);
                    projectname = user.get("nameproj").toString();
                    ChefProj = user.get("ChefProj").toString();
                    descrProj = user.get("description_pro").toString();
                    avancementProj = user.get("avancement_pro").toString();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                ImageView disable = (ImageView) findViewById(R.id.disableProject);
                TextView idProj_ = (TextView)findViewById(R.id.idProj);
                TextView nameProj_ = (TextView)findViewById(R.id.nameProj);
                TextView descProj_ = (TextView)findViewById(R.id.descProj);
                TextView avancementProj_ = (TextView)findViewById(R.id.avancementProj);
                TextView ChefdeProj_ = (TextView)findViewById(R.id.ChefDeProj);
                TextView Title_ = (TextView)findViewById(R.id.titleInfoProj);
                Title_.setText(projectname);
                idProj_.setText(idProj);
                nameProj_.setText(projectname);
                descProj_.setText(descrProj);
                avancementProj_.setText(avancementProj);
                ChefdeProj_.setText(ChefProj);
                ProgressBar Prog = (ProgressBar) findViewById(R.id.progressBarProjet);
                Prog.setProgress((int)Double.parseDouble(avancementProj));
                if(Admin.equals("1")) {
                    disable.setVisibility(View.VISIBLE);
                }
        }
        if(FLAG == 12){
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                }
            }, 2000);
        }
    }

    public void onClickDeleteProj(View view){
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:
                        BackGroundMethodDisableProj();
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete This Project").setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public void BackGroundMethodDisableProj(){
        String method = "DeleteProj";
        BackgroundTasks asyncTask =new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method,idProj);
    }
}
