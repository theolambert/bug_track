package com.example.theolambert.bugtrace;

/**
 * Created by theolambert on 06/07/16.
 */
public interface AsyncResponse {
    void processFinish(String code, String result, int FLAG, String msg);
}
