package com.example.theolambert.bugtrace;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Project extends AppCompatActivity implements AsyncResponse {
    BackgroundTasks asyncTask = new BackgroundTasks(this);
    private String method = "project";
    private String[] tab = null;
    private String  ChefProj = "", avancementProj = "", dateCreaProj= "", descrProj = "", project = "", idProjet = "", idTeam = "";
    private ArrayList<String> items = null;
    private String [] nameproj = null, idProj = null;
    private String id_info = null;
    private String Admin = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_project);
            Intent myIntent = getIntent();
            if(myIntent != null)
            {
                id_info = myIntent.getStringExtra("id");
                Admin = myIntent.getStringExtra("Admin");
                idTeam = myIntent.getStringExtra("Team");
                asyncTask.delegate = this;
                asyncTask.execute(method, id_info);
            }
    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg){
        final ListView lv = (ListView) findViewById(R.id.List);
        if (code.equals("0")){
            Log.d("TAG JSON ----->", " " + result);
            try {
                JSONArray users = new JSONArray(result);
                JSONObject user;
                nameproj = new String[users.length()];
                idProj = new String[users.length()];
                items = new ArrayList<>();
                for (int i = 0; i < users.length(); i++) {
                    user = users.getJSONObject(i);
                    nameproj[i] = user.get("nameproj").toString();
                    ChefProj = user.get("ChefProj").toString();
                    idProj[i] = user.get("idProj").toString();
                    items.add(nameproj[i] + " coordinate by " + ChefProj);
                    Log.d("TAG PROJ{" + nameproj[i] + "} ----->", "DONE!");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            ArrayAdapter<String> mArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_expandable_list_item_1, items);
            assert lv != null;
            lv.setAdapter(mArrayAdapter);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                TextView txt = (TextView) findViewById(R.id.txt);
                TextView txt2 = (TextView) findViewById(R.id.txt2);
                TextView txt3 = (TextView) findViewById(R.id.txt3);

                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    project = nameproj[position];
                    idProjet = idProj[position];
                    txt.setText(project+" ?");
                    txt.setVisibility(View.VISIBLE);
                    txt2.setText(project+ " ?");
                    txt2.setVisibility(View.VISIBLE);
                    txt3.setText(project+ " ?");
                    txt3.setVisibility(View.VISIBLE);
                    findViewById(R.id.BugButton).setVisibility(View.VISIBLE);
                    findViewById(R.id.BugButtonTr).setVisibility(View.INVISIBLE);
                    findViewById(R.id.EyeButton).setVisibility(View.VISIBLE);
                    findViewById(R.id.EyeButtonTr).setVisibility(View.INVISIBLE);
                    findViewById(R.id.Module).setVisibility(View.VISIBLE);
                    findViewById(R.id.Moduletr).setVisibility(View.INVISIBLE);

                }
            });
        } else if (code.equals("2")){
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        finish();
                    }
                }, 2000);
        }
    }

    public void BugOnclick(View view){
        Intent intent = new Intent(this, Bug.class);
        intent.putExtra("idProj",idProjet);
        intent.putExtra("projectname",project);
        intent.putExtra("idTeam",idTeam);
        intent.putExtra("idUser",id_info);
        this.startActivity(intent);
    }

    public void DescOnClick(View view){
        Intent intent = new Intent(this, ProjectInfo.class);
        intent.putExtra("idProj",idProjet);
        intent.putExtra("Admin",Admin);
        this.startActivity(intent);
    }

    public void ModuleOnClick(View view){
        Intent intent = new Intent(this, ProjectModule.class);
        intent.putExtra("idProj",idProjet);
        intent.putExtra("projectname",project);
        this.startActivity(intent);
    }

    public void Refresh(View view){
        finish();
        startActivity(getIntent());
    }
}

