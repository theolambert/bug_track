package com.example.theolambert.bugtrace;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class CreateAccount extends AppCompatActivity implements AsyncResponse {
    EditText name_user, prenom_user, email_user, mdp_user;
    ImageView title;
    String name_, prenom_, email_, mdp_, team_ = null, CurrentTeam_, Admin_ = String.valueOf(0);
    CheckBox chkbox;
    Spinner spinner;
    List exemple;
    String [] tabTeam = null;
    String [] idProjectCA = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        BackgroundTasks asyncTask = new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute("allTeam");
        spinner = (Spinner) findViewById(R.id.spinner);
        name_user = (EditText) findViewById(R.id.name);
        prenom_user = (EditText) findViewById(R.id.prenom);
        email_user = (EditText) findViewById(R.id.mail);
        mdp_user = (EditText) findViewById(R.id.mdp);
        chkbox = (CheckBox)findViewById(R.id.checkBox);
        title = (ImageView) findViewById(R.id.Okregister);
        final Animation animation = new AlphaAnimation((float) 1, (float) 0.2);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        title.startAnimation(animation);
    }

    public void onRegister(View view) {
        name_ = name_user.getText().toString();
        prenom_ = prenom_user.getText().toString();
        mdp_ = mdp_user.getText().toString();
        email_ = email_user.getText().toString();
        if(chkbox.isChecked()){
            Admin_ = String.valueOf(1);
        }
        String method = "register";
        if (name_.matches("") || prenom_.matches("") || mdp_.matches("") || email_.matches("")) {
            Toast.makeText(this, "All field required !", Toast.LENGTH_LONG).show();

        }
        if (!name_.matches("") && !prenom_.matches("") && !mdp_.matches("") && !email_.matches("")) {
            BackgroundTasks asyncTask = new BackgroundTasks(this);
            asyncTask.delegate = this;
            asyncTask.execute(method, name_, prenom_, mdp_, email_, String.valueOf(team_), Admin_);
        }

    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg) {
        if (FLAG == 1) {
            if (code.equals("0")) {
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
                finish();
            } else if (code.equals("2")) {
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            }

        }
        if (FLAG == 9) {
            if (code.equals("0")) {
                SetMySpinner(result);
            }
        }
    }

    public void SetMySpinner(String res){
        try {
            JSONArray users = new JSONArray(res);
            JSONObject user;
            exemple = new ArrayList();
            idProjectCA = new String [users.length()];
            for (int i = 0; i < users.length(); i++) {
                user = users.getJSONObject(i);
                idProjectCA[i] = user.get("id_team").toString();
                String team = user.get("name_team").toString();
                exemple.add(team);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ArrayAdapter adapter = new ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                exemple
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                team_ = idProjectCA[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //
            }
        });
    }
}




