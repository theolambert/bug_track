package com.example.theolambert.bugtrace;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Home extends AppCompatActivity implements AsyncResponse {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    FragmentTransaction fragmentTransaction;
    NavigationView navigationView;
    String prenom_user_ = "", nom_user_ = "", isAdmin_ = "", team_ = "";
    private String userInfo = null;
    private String[] User = null;
    protected void onCreate(Bundle savedInstanceState) {
        Intent myIntent = getIntent();
        if(myIntent != null) {
            userInfo = myIntent.getStringExtra("UserInfoArray");
            try {
                JSONArray users = new JSONArray(userInfo);
                JSONObject user;
                user = users.getJSONObject(0);
                User = new String[7];
                User [0] = user.get("id").toString();
                User [1] = user.get("name").toString();
                User [2] = user.get("prenom").toString();
                User [3] = user.get("mdp").toString();
                User [4] = user.get("mail").toString();
                User [5] = user.get("admin").toString();
                User [6] = user.get("team").toString();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            for (int i = 0; i < 6; i++) {
                Log.d("TAG", "INFO USER: "+User[i]);
            }

        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connection_success);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigationView = (NavigationView) findViewById(R.id.navigation_view);
        navigationView.setItemIconTintList(null);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.main_container, new GeneralFragment());
        fragmentTransaction.commit();
        getSupportActionBar().setTitle("General");
        navigationView = (NavigationView)findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.General:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container, new GeneralFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("General");
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.Settings:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container, new SettingsFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Settings");
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.Twitter:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container, new Twitter());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Twitter");
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.GitHub:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container, new GitFragment());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("GitHub");
                        drawerLayout.closeDrawers();
                        break;
                    case R.id.Disable:
                        fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.replace(R.id.main_container, new DisableAccount());
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle("Delete Account");
                        drawerLayout.closeDrawers();
                        break;
                }
                return false;
            }
        });
}

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        actionBarDrawerToggle.syncState();

    }

    public void TeamConnect(View view){
        Intent intent = new Intent(this, Team.class);
        intent.putExtra("team", User[6]);
        intent.putExtra("Admin", User[5]);
        startActivity(intent);
    }

    public void ProjectConnect(View view){
        Intent intent = new Intent(this, Project.class);
        intent.putExtra("id", User[0]);
        intent.putExtra("Admin", User[5]);
        intent.putExtra("Team", User [6]);
        startActivity(intent);
    }

    public void GoPassActivity(View view) {
        Intent intent = new Intent(this, ChangePassword.class);
        intent.putExtra("id", User[0]);
        intent.putExtra("pass", User[3]);
        startActivity(intent);
    }

    public void UpdateAccount(View view) {
        EditText nom = (EditText)findViewById(R.id.Nom_user);
        EditText prenom = (EditText)findViewById(R.id.Prenom_user);
        EditText team = (EditText)findViewById(R.id.Team_user);
        String nom_user = nom.getText().toString();
        String prenom_user = prenom.getText().toString();
        String team_user = team.getText().toString();
        User [1] = nom_user_;
        User [2] = prenom_user_;
        User [6] = team_;
        if(nom_user.matches("") || prenom_user.matches("") ||  team_user.matches("")){
            Toast.makeText(this, "All field required !", Toast.LENGTH_LONG).show();
        }
        if (!nom_user.matches("") && !prenom_user.matches("") && !team_user.matches("")) {
            String method = "update";
            String id_user = User[0];
            BackgroundTasks asyncTask =new BackgroundTasks(this);
            asyncTask.delegate = this;
            asyncTask.execute(method,id_user, nom_user, prenom_user, team_user);
        }

    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg){

        if(FLAG == 5){
            Toast.makeText(Home.this, msg, Toast.LENGTH_LONG).show();
        }

        if (FLAG == 8) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Toast.makeText(Home.this, "Must reconnect you !", Toast.LENGTH_LONG).show();
                    finish();
                }
            }, 3000);
        }
        if (FLAG == 14){
            try {
                JSONArray users = new JSONArray(result);
                JSONObject user;
                user = users.getJSONObject(0);
                nom_user_ = user.get("nom_user").toString();
                prenom_user_ = user.get("prenom_user").toString();
                team_ = user.get("id_equipe").toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            EditText nom = (EditText)findViewById(R.id.Nom_user);
            EditText prenom = (EditText)findViewById(R.id.Prenom_user);
            EditText team = (EditText)findViewById(R.id.Team_user);
            nom.setText(nom_user_);
            prenom.setText(prenom_user_);
            team.setText(team_);
        }
    }

    public void SetMyInfo(View view){
        BackGroundMethodHome("CurrentInfo");
    }

public void DeleteMyAccount(View view){
    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            switch (which){
                case DialogInterface.BUTTON_POSITIVE:
                    BackGroundMethodHome("Delete");
                    break;

                case DialogInterface.BUTTON_NEGATIVE:
                    //No button clicked
                    break;
            }
        }
    };

    AlertDialog.Builder builder = new AlertDialog.Builder(this);
    builder.setTitle("Supress your Account").setMessage("Are you sure?").setPositiveButton("Yes", dialogClickListener)
            .setNegativeButton("No", dialogClickListener).show();

    }

    public void BackGroundMethodHome(String method){
        String id_user = User[0];
        BackgroundTasks asyncTask =new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method,id_user);
    }

    public void BugResolve(View view){
        Intent intent = new Intent(this, BugToResolve.class);
        intent.putExtra("id", User[0]);
        intent.putExtra("Admin", User[5]);
        startActivity(intent);
    }

    public void BugPosted(View view){
        Intent intent = new Intent(this, BugPosted.class);
        intent.putExtra("id", User[0]);
        intent.putExtra("Admin", User[5]);
        startActivity(intent);
    }

}
