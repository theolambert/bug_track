package com.example.theolambert.bugtrace;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Bug extends AppCompatActivity implements AsyncResponse,  AdapterView.OnItemSelectedListener {
    private Spinner spinner; //FIRST SPINNER
    private Spinner spinner2; //SECOND SPINNER
    private Spinner spinner3; //THIRD SPINNER
    private Spinner spinner4; //FOURTH SPINNER
    private static final String[]paths = {"Blocker", "Critical", "Major", "Minor", "Trivial"}; //PATHS FOR PRIORITY, IMPLEMENTS SPINNER
    private static final String[]pathsState = {"New", "Accept", "Resolved", "Closed"}; //PATHS FOR PRIORITY, IMPLEMENTS SPINNER
    private String TitleBug= null; //BUG TITLE
    private String DescBug= null; //DESCPRIPTION TITLE
    private String idProj = null; //ID PROJECT
    private String idTeam = null; //ID TEAM
    private String idUser = null; //ID USER
    private String resState = null; //RESPONSE OF THE SPINNER STATE
    private String resEtat = null; //RESPONSE OF THE SPINNER PRIORITY
    private String resModule = null; //MODULE SELECTED IN THE SPINNER
    private String resUser = null; //USER WHO ARE GOING TO GET AFFECTED
    private String Avancement = null; //PROGRESSION
    private String [] idUserToReport = null; //TAB
    private String [] idModuleToReport = null; //TAB
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bug);
        Intent myIntent = getIntent();
        String projectname = myIntent.getStringExtra("projectname");
        idProj = myIntent.getStringExtra("idProj");
        idUser = myIntent.getStringExtra("idUser");
        idTeam = myIntent.getStringExtra("idTeam");
        String method = "team";
        BackgroundTasks asyncTask =new BackgroundTasks(this);
        asyncTask.delegate = this;
        asyncTask.execute(method, idTeam);
        TextView title = (TextView) findViewById(R.id.TitleProjectCurrent);
        title.setText(projectname);

        //SPINNER 1
        spinner = (Spinner) findViewById(R.id.spinnerBug);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Bug.this,
                android.R.layout.simple_spinner_item, paths);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(Bug.this);


        //SPINNER 3
        spinner3 = (Spinner) findViewById(R.id.spinner3);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Bug.this,
                android.R.layout.simple_spinner_item, pathsState);

        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(adapter3);
        spinner3.setOnItemSelectedListener(Bug.this);
        SeekBar seekBar = (SeekBar) findViewById(R.id.seekbarBug);
        final TextView seekBarValue = (TextView) findViewById(R.id.seekbarvalueBug);
        assert seekBar != null;
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser) {
                seekBarValue.setText(String.valueOf(progress));
                Avancement = String.valueOf(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //DO NOTHING
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //DO NOTHING
            }
        });
    }

    //GET ALL VALUE REQUIRED IN THE ACVTIVITY AND ARE GOING TO BE SENT TO THE DATABASE
    public void onClickBug(View view){
        EditText TitlebugProj = (EditText)findViewById(R.id.editTextTitle);
        EditText DescBugProj = (EditText)findViewById(R.id.etMessageBox);
        TitleBug = TitlebugProj.getText().toString();
        DescBug = DescBugProj.getText().toString();
        if(TitleBug.matches("") || DescBug.matches("")){
            Toast.makeText(this, "All field required !", Toast.LENGTH_LONG).show();

        }
        if (!TitleBug.matches("") && !DescBug.matches("")){
        {
            String method = "Bug_report";
            BackgroundTasks asyncTask =new BackgroundTasks(this);
            asyncTask.delegate = this;
            asyncTask.execute(method,TitleBug, DescBug, resState, idUser, resEtat, Avancement, String.valueOf(resUser), resModule);
        }

    }}


    @Override
    //AT THE END OF THE ASYNCTASK RESPONSE, WE GET THE CODE, THE RESULT,
    //THE FLAG AND MESSAGE OF THE DATA BASE AND WE ARE GOING TO WORK ON IT
    public void processFinish(String code, String result, int FLAG, String msg){
        if(FLAG == 7){
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }
        if(FLAG == 3){
            SetMySpinner2(result);
            BackgroundTasks asyncTask =new BackgroundTasks(this);
            String method = "YourModule";
            asyncTask.delegate = this;
            asyncTask.execute(method, idProj);
        }
        if(FLAG == 19){
            SetMySpinner4(result, code);
        }
    }

    @Override

    //GET RESULT OF EACH SPINNER
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        if(spinner.getId() == R.id.spinnerBug)
        {
            resEtat = parent.getItemAtPosition(position).toString();
        }
        else if(spinner.getId() == R.id.spinnerBug2)
        {
            resUser = idUserToReport[position];
        }
        else if(spinner.getId() == R.id.spinner3)
        {
            resState = parent.getItemAtPosition(position).toString();
        }
        else if(spinner.getId() == R.id.spinner4)
        {
            resModule = idModuleToReport[position];
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // DO NOTHING
    }

    public void SetMySpinner2(String res){
        //GET THE JSON RESPONSE OF THE REQUEST
        String [] emailUser = null;
        //TRY TO CAST JSON TO GET SEVERAL KEY/VALUE TO DISPLAY THE IN SPINNER
        try {
            JSONArray users = new JSONArray(res);
            JSONObject user;
            emailUser = new String [users.length()];
            idUserToReport = new String [users.length()];
            for (int i = 0; i < users.length(); i++) {
                user = users.getJSONObject(i);
                emailUser[i] = user.get("mail").toString();
                idUserToReport[i] = user.get("id").toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        spinner2 = (Spinner) findViewById(R.id.spinnerBug2);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Bug.this,
                android.R.layout.simple_spinner_item, emailUser);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);
        spinner2.setOnItemSelectedListener(Bug.this);
    }

    //SET THE 4TH SPINNER
    public void SetMySpinner4(String res, String code){
        if (!code.equals("2")){
            String [] Module = null;
            try {
                JSONArray users = new JSONArray(res);
                JSONObject user;
                Module = new String [users.length()];
                idModuleToReport = new String [users.length()];
                for (int i = 0; i < users.length(); i++) {
                    user = users.getJSONObject(i);
                    Module[i] = user.get("name_module").toString();
                    idModuleToReport[i] = user.get("id_module").toString();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            //SPINNER 4
            spinner4 = (Spinner) findViewById(R.id.spinner4);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(Bug.this,
                    android.R.layout.simple_spinner_item, Module);

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner4.setAdapter(adapter);
            spinner4.setOnItemSelectedListener(Bug.this);
        }else{
            Toast.makeText(this, "/WARNING NO MODULE/", Toast.LENGTH_LONG).show();
        }
    }
}