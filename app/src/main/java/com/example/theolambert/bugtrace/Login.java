package com.example.theolambert.bugtrace;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;


public class Login extends AppCompatActivity implements AsyncResponse {
    EditText email_user, mdp_user;
    public String email_, mdp_;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        email_user = (EditText) findViewById(R.id.Email);
        mdp_user = (EditText) findViewById(R.id.Password);
    }

    public void onClick(View view) {
        Intent intent = new Intent(Login.this, CreateAccount.class);
        startActivity(intent);
    }

    public void onClickConnect(View view) {
        BackgroundTasks asyncTask = new BackgroundTasks(this);
        mdp_ = mdp_user.getText().toString();
        email_ = email_user.getText().toString();
        String method = "login";
        asyncTask.delegate = this;
        asyncTask.execute(method, email_, mdp_);
    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg){
        Intent intent = new Intent(this, Home.class);
        intent.putExtra("UserInfoArray", result);
        startActivity(intent);
    }
}
