package com.example.theolambert.bugtrace;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class ChangePassword extends AppCompatActivity implements AsyncResponse {
    private String current_pass = null; //CURRENT PASSWORD
    private String current_id = null; //CURRENT ID DU USER
    private String method = "pass"; //METHOD POUR L'ASYNCTASK
    private String oldpass = null; //OLD PASSWORS
    private String confirmpass = null; //CONFIRM PASS
    private String newpass = null; //NEW PASS
    BackgroundTasks asyncTask =new BackgroundTasks(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
    }
    //ON CLICK POUR SET LE NEW PASS
    public void onClickExit(View v){
        Intent myIntent = getIntent();
        if(myIntent != null) {
            current_id = myIntent.getStringExtra("id");
            current_pass = myIntent.getStringExtra("pass");
        }
        EditText old_pass = (EditText)findViewById(R.id.OldPass);
        EditText new_pass = (EditText)findViewById(R.id.NewPass);
        EditText confirm_pass = (EditText)findViewById(R.id.ConfirmPass);
        newpass = new_pass.getText().toString();
        confirmpass = confirm_pass.getText().toString();
        oldpass = old_pass.getText().toString();
        ImageView unlock = (ImageView)findViewById(R.id.unlocked);
        ImageView lock = (ImageView)findViewById(R.id.locked);

        //SI LES FIELDS NE SONT PAS VIDE
        if (!newpass.matches("") && !confirmpass.matches("") && !oldpass.matches("")) {
            //SI LE CURRENT PASS EST EGAL A L'ANCIEN MOT DE PASSE
            if(current_pass.equals(oldpass)){
                //SI LE NEW PASS EST EGAL AU CONFIRM PASS
                if (newpass.equals(confirmpass)){
                    asyncTask.delegate = this;
                    asyncTask.execute(method, current_id,confirmpass);
                    unlock.setVisibility(View.INVISIBLE);
                    lock.setVisibility(View.VISIBLE);
                }
                else {
                    Toast.makeText(this, "MissMatch !", Toast.LENGTH_LONG).show();
                }
            }else{
                Toast.makeText(this, "It's not your current pass !", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(this, "All field required !", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void processFinish(String code, String result, int FLAG, String msg){
        ImageView lock = (ImageView)findViewById(R.id.locked);
        final Animation animation = new AlphaAnimation((float) 1,(float) 0.1);
        animation.setDuration(1000);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        lock.startAnimation(animation);

        Handler handler = new Handler();
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        handler.postDelayed(new Runnable() {
            public void run() {
                Intent intent = new Intent(ChangePassword.this, Login.class);
                startActivity(intent);
                Toast.makeText(ChangePassword.this, "Must reconnect you !", Toast.LENGTH_LONG).show();
            }
        }, 3500);
    }
}
