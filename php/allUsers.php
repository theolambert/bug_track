<?php

	header('Content-type: text/plain; charset=utf-8');
	
	require "init.php";
	
	$sql_query = "SELECT  nom_user ,  prenom_user ,  email_user , name_team, name_role
					FROM USERS
					INNER JOIN TEAM ON TEAM.id_team = USERS.id_equipe
					INNER JOIN ROLE ON ROLE.id_role = USERS.role_user";
	$result = $con->query($sql_query);

	if(mysqli_query($con, $sql_query))
	{	
		if ($result->num_rows > 0) {
			$index = array();
			while($row = $result->fetch_assoc()) {
				$index[] =array('nomUser' => $row["nom_user"], 'prenomUser' => $row["prenom_user"], 'emailUser' => $row["email_user"], 'nameTeam' => $row["name_team"], 'nameRole' => $row["name_role"]);
			}
			echo json_encode($index);

		} else  {
			echo "0 results";
		}

		$con->close();

	} else {
		echo "error".mysqli_error($con);
	}

?>

